#!/usr/bin/env python3
# -*- encoding:utf-8 -*-

"""
Usage: toolbox.py TOOL ACTION [options]
       toolbox.py -h | --help
       toolbox.py  --version

Install or update personal dotfiles, services or macros.

Arguments:
  ACTION                action to perform (install, update, add, etc.)
  TOOL                  software to consider

Options:
  --version             Show version
  -h --help             Show this screen
  -q --quiet            Quiet mode
  -t --timeout=<value>  Kill process after timeout in seconds
  --args=<args>         All arguments to give to sub-scripts
"""
import toolbox
import subprocess
import os
from utils import get_toolboxdir
from docopt import docopt


def run(tool, action, timeout=None, quiet=False, args=None):

    script_sh = os.path.join(TOOLBOXDIR, tool, action + '.sh')
    script_py = os.path.join(TOOLBOXDIR, tool, action + '.py')

    if os.path.isfile(script_sh):
        interpreter = 'bash'
        script = script_sh
    elif os.path.isfile(script_py):
        interpreter = 'python3'
        script = script_py
    else:
        cmd = "toolbox %s %s" % (tool, action)
        print("There is no file to run this action : %s" % cmd)
        return 1

    if not quiet:
        resp = subprocess.run([interpreter, script],
                              timeout=timeout)
    else:
        resp = subprocess.run([interpreter, script],
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              universal_newlines=True)

    return resp


if __name__ == '__main__':
    args = docopt(__doc__)

    TOOLBOXDIR = get_toolboxdir()

    # Get tool
    if args['TOOL']:
        tool = args['TOOL']

    # Get action
    if args['ACTION']:
        action = args['ACTION']

    # Get options:
    timeout = args['--timeout']
    quiet = args['--quiet']

    # Execution
    run(tool, action, timeout=timeout, quiet=quiet, args=args['--args'])
