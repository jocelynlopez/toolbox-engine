#!/bin/bash

function ProgressBar {

    let _total_done=$(tput cols)-${#1}-${#2}-51
    _total_fill=$(printf "%${_total_done}s")
    
    if [ $3 -eq $4 ];then
        printf "\r$1 : [########################################] 100%% $2${_total_fill// / }"
    elif [ $3 -eq 0 ];then
        printf "\r$1 : [----------------------------------------]   0%% $2${_total_fill// / }"
    else
        let _progress=(${3}*100/${4}*100)/100
        let _done=(${_progress}*4)/10
        let _left=40-$_done
        _fill=$(printf "%${_done}s")
        _empty=$(printf "%${_left}s")
        printf "\r$1 : [${_fill// /#}${_empty// /-}] ${_progress}%% $2${_total_fill// / }"
    fi
    sleep 0.2
}
