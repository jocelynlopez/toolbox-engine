#!/usr/bin/env python

from ..utils import process_template

process_template("rsnapshot.conf.template",
                 "default.json",
                 "/etc/rsnapshot.conf")
