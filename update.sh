#!/bin/bash

if [ -z "$TOOLBOXDIR" ]; then
    echo "TOOLBOXDIR environment variable is unset !";
    while true; do
        read -p "Do you wish to install TOOLBOXDIR variable in bashrc set to : $PWD ?" yn
        case $yn in
            [Yy]* ) echo "export TOOLBOXDIR=$PWD" >> ~/.bashrc; echo "You need to restart this shell"; break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

else
    cd $TOOLBOXDIR
    git pull
fi

