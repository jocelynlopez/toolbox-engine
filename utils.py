#!/usr/bin/env python3

import os
import json
import sys
from jinja2 import Template


def get_toolboxdir():
    # Check environment variable TOOLBOXDIR exist
    if 'TOOLBOXDIR' in os.environ:
        TOOLBOXDIR = os.environ['TOOLBOXDIR']
    else:
        print("TOOLBOXDIR environment variable is not set ! Aborting...")
        sys.exit(1)
    return TOOLBOXDIR


def process_template(template, config, dest):
    with open(config, "r") as f_conf:
        var_template = json.load(f_conf)

    with open(template, "r") as f_template:
        template = Template(f_template.read())

    with open(dest, "w") as conf:
        conf.write(template.render(**var_template))
