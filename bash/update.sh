#!/bin/bash:wq

set -e
source $TOOLBOXDIR/utils.sh

ProgressBar "Update bash" "Update powerline-shell" 0 3
cd $TOOLBOXDIR/bash/powerline-shell
git pull >> /dev/null 2>&1
cp $TOOLBOXDIR/bash/powerline-shell-config.py $TOOLBOXDIR/bash/powerline-shell/config.py

ProgressBar "Update bash" "Re-install of powerline-shell" 1 3
./install.py >> /dev/null 2>&1

ProgressBar "Update bash" "Copy .bashrc file" 2 3
# Copy .bashrc file
cp $TOOLBOXDIR/bash/bashrc.template ~/.bashrc
sed -i "s|{{TOOLBOXDIR}}|$TOOLBOXDIR|g" ~/.bashrc

ProgressBar "Update bash" "" 3 3
printf '\n'
