#!/bin/bash

set -e
source $TOOLBOXDIR/utils.sh

ProgressBar "Installation de bash" "Mise à jour du dépôt Git" 0 3 
cd $TOOLBOXDIR/bash
if [ ! -d "$TOOLBOXDIR/bash/powerline-shell" ];then
	git clone https://github.com/milkbikis/powerline-shell.git >> /dev/null 2>&1 
fi
cd $TOOLBOXDIR/bash/powerline-shell
git pull >> /dev/null 2>&1
cp $TOOLBOXDIR/bash/powerline-shell-config.py $TOOLBOXDIR/bash/powerline-shell/config.py

ProgressBar "Installation de bash" "Installation de Powerline-Shell" 1 3 
./install.py >> /dev/null 2>&1

# Copy .bashrc file
ProgressBar "Installation de bash" "Copie de bashrc" 2 3 
cp $TOOLBOXDIR/bash/bashrc.template ~/.bashrc
sed -i "s|{{TOOLBOXDIR}}|$TOOLBOXDIR|g" ~/.bashrc

ProgressBar "Installation de bash" "" 3 3 
printf '\n'

