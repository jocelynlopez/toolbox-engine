#!/bin/bash

# Copy .vimrc file
cp $TOOLBOXDIR/vim/vimrc ~/.vimrc

# Copy ftplugin dir
cp -r $TOOLBOXDIR/vim/after ~/.vim/

# Copy colors dir
cp -r $TOOLBOXDIR/vim/colors ~/.vim/

# Install all plugins
vim +PluginInstall +qall

