#!/bin/bash

set -e
source $TOOLBOXDIR/utils.sh

# Install vim
ProgressBar "Installation vim" "installation vim" 0 3
apt-get install vim -y >> /dev/null 2>&1

ProgressBar "Installation vim" "copy .vimrc and ftplugin dir" 1 3
# Copy .vimrc file
cp $TOOLBOXDIR/vim/vimrc ~/.vimrc
# Copy ftplugin dir
cp -r $TOOLBOXDIR/vim/after ~/.vim/
# Copy colors dir
cp -r $TOOLBOXDIR/vim/colors ~/.vim/

# Install plugin manager if needed
ProgressBar "Installation vim" "Installation of all plugins" 2 3
if [ ! -d "~/.vim/bundle/Vundle.vim" ];then
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim >> /dev/null 2>&1
fi
# Install all plugins
vim +PluginInstall +qall /dev/null 2>&1
# plugin : YouCompleteMe (YCM)
cd ~/.vim/bundle/youcompleteme/
apt-get install build-essential cmake -y >> /dev/null 2>&1
./install.py >> /dev/null 2>&1
# plugin : Syntastic
pip install flake8 >> /dev/null 2>&1
# plugin : vim-autoformat
pip install autopep8 >> /dev/null 2>&1
# plugin : powerline
cd $TOOLBOXDIR/vim
wget --quiet https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf >> /dev/null 2>&1
mv PowerlineSymbols.otf ~/.local/share/fonts/
fc-cache -f >> /dev/null 2>&1
wget --quiet https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf >> /dev/null 2>&1
if [ ! -d "~/.config/fontconfig/fonts.conf" ];then
	mkdir -p ~/.config/fontconfig/fonts.conf
fi
mv 10-powerline-symbols.conf ~/.config/fontconfig/fonts.conf/
# You need to use a 24bit color terminal for using correctly powerline status...(like gnome-terminal)"

ProgressBar "Installation vim" "" 3 3

