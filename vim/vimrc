set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'valloric/youcompleteme'
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/syntastic'
Plugin 'chiel92/vim-autoformat'
Plugin 'kien/ctrlp.vim'
Plugin 'sirver/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'terryma/vim-expand-region'

" All of your Plugins must be added before the following line
call vundle#end()            " required


" ===================================================================================
" ===================================================================================
"  				  General VIM options
" ===================================================================================
" ===================================================================================

set clipboard=unnamed 					" use system clipboard not only vim clipboard

" --------------------------------
" Keys stroke
" --------------------------------
let mapleader = "\<Space>"				" set leader key
set backspace=indent,eol,start				" use classic behavior of Backslash
" alternative to <Esc>
imap ,; <Esc>
map ,; <Esc>
" deactivate arrow
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

" --------------------------------
" Files
" --------------------------------
set encoding=utf-8					" use utf-8 encoding
au BufNewFile,BufRead *.comm set filetype=python	" highlight code_aster file as python file
set hidden						" hide files when opening an other file

" --------------------------------
" View
" --------------------------------
filetype plugin indent on    				" activate file specific behavior like syntax/indentation
set nu							" add absolute line numbers of the cursor
set relativenumber					" set relative numbers
set wrap						" wrap to long lines
syntax on 						" activate syntax highlighting
set background=light					" set dark/light background
colorscheme solarized					" set colorscheme
set guifont=DejaVu\ Sans\ Mono\ 10			" set font
set antialias						" set antialiasing on font

" --------------------------------
" Research
" --------------------------------
set ignorecase						" ignore the case when searching
set smartcase						" if a search contains a capital, re-activates the case sensitive
set incsearch						" highlight search results as you type
"set hlsearch						" highlight search results

" --------------------------------
" Beep
" --------------------------------
set visualbell						" don't let vim beep
set noerrorbells					" don't let vim beep

" --------------------------------
" Selection
" --------------------------------
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" ------------------------------------------------------------------------------------
" 				   Feature : Status Bar
"
" 	required : 
" 		Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
" ------------------------------------------------------------------------------------
set laststatus=2

" ------------------------------------------------------------------------------------
" 			   Feature : File Tree / Research files
"			   ------------------------------------
" 	required : 
" 		Plugin 'scrooloose/nerdtree'
" 		Plugin 'jistr/vim-nerdtree-tabs'
" 		Plugin 'kien/ctrlp.vim'

" 	commands:
" 		<leader>n 			" toggle NERDTree
" ------------------------------------------------------------------------------------
let NERDTreeIgnore=['\.pyc$', '__pycache__', '\~$'] 		" ignore files in NERDTree
nnoremap <Leader>n :NERDTreeTabsToggle<CR>			" open in all tabs / close in all tabs
nnoremap <leader>o :CtrlPMixed<CR>				" open ctrlp to search in Files, Buffers and MRU files at the same time

" ------------------------------------------------------------------------------------
" 				   Feature : Comment
"
" 	required : 
"		Plugin 'scrooloose/nerdcommenter'
" 	commands:
" 		<leader>c<space>		" comment/uncomment line or block lines
" 		<leader>c$			" comments the current line from the cursor to the end of line
" ------------------------------------------------------------------------------------
let g:NERDDefaultAlign='left'

" ------------------------------------------------------------------------------------
" 				   Feature : Backup
" ------------------------------------------------------------------------------------
"set backup
"set writebackup
"set backupdir=~/.vim/backup
"set directory=~/.vim/tmp

" ------------------------------------------------------------------------------------
" 				   Feature : Layouts
" ------------------------------------------------------------------------------------
set splitbelow					" add a vertical layout below
set splitright					" add an horizontal layout at right

"split navigations
nnoremap <C-J> <C-W><C-J>			" move to the below layout
nnoremap <C-K> <C-W><C-K>			" move to the upper layout
nnoremap <C-L> <C-W><C-L>			" move to the right layout
nnoremap <C-H> <C-W><C-H> 			" move to the left layout

" ===================================================================================
" ===================================================================================
"  				      Python IDE
" ===================================================================================
" ===================================================================================
" rope : refactoring
" tagbar
" delimitMate
" colorizer
" matchit
" tabular
" nextval
" surround

" Features:
" ----------------
" 1.  autocompletion				: OK
" 2.  show GOTO definition of a function/class	: OK
" 3.  show function/class documentation		: OK
" 4.  git integration				: OK
" 5.  git gutter				: OK
" 6.  comment/uncomment line or block lines 	: OK
" 7.  pep8 autoformat				: OK
" 8.  delete trailing space at saving		: OK
" 9.  snippets 					: NOK
" 10. some django features			: NOK
" 11. hightlighting file : ansys, docker, ini	: NOK
" 12. diff file					: NOK
" 13. markdown and rst file preview		: NOK
" 14. refactoring 				: NOK
"
" ------------------------------------------------------------------------------------
" 				   Feature : 7, 8:
"				   ---------------
" 	required : 
" 		Plugin 'vim-scripts/indentpython.vim'
" 		Plugin 'chiel92/vim-autoformat'
" ------------------------------------------------------------------------------------
au BufWrite *.py :Autoformat
" add here ignore error for autopep8

" ------------------------------------------------------------------------------------
"  	   Features : Autocomplete / GoToDefinition / GetDoc / Snippets
"          ------------------------------------------------------------
" 	required : 
" 		Plugin 'valloric/youcompleteme'
"	commands :
"		<leader>g 					" Go to definition
"		<leader>d 					" Get documentation
"		<Ctrl>o 					" Go back to initial file, useful if GoTo is on same-buffer
" ------------------------------------------------------------------------------------
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
map <leader>d  :YcmCompleter GetDoc<CR>
let g:ycm_goto_buffer_command = 'new-tab' 			" where to go when hit GoTo, choices : [ 'same-buffer', 'horizontal-split', 'vertical-split', 'new-tab', 'new-or-existing-tab' ]
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_filetype_blacklist = {
      \ 'notes' : 1,
      \ 'markdown' : 1,
      \ 'text' : 1,
      \}
let g:ycm_disable_for_files_larger_than_kb = 1000 		" disable YCM if file larger than specified in kB
"" make YCM compatible with UltiSnips (using supertab)
"let g:ycm_key_list_select_completion = '<leader><tab>' 
"let g:ycm_key_list_previous_completion = '<leader><s-tab>'
"let g:ycm_key_list_select_completion = '<c-n>' 
"let g:ycm_key_list_previous_completion = '<c-p>'

" ------------------------------------------------------------------------------------
"  	   			Features : Snippets
"          			-------------------
" 	required : 
" 		Plugin 'sirver/ultisnips'
" 		Plugin 'honza/vim-snippets'
"	commands :
"		<Ctrl>o 					" Go back to initial file, useful if GoTo is on same-buffer
" ------------------------------------------------------------------------------------
let g:UltiSnipsExpandTrigger = "<leader><tab>"
let g:UltiSnipsJumpForwardTrigger = "<leader><tab>"
let g:UltiSnipsJumpBackwardTrigger = "<leader><s-tab>"

" ------------------------------------------------------------------------------------
" 		   	   Features : Syntax Checking/Highlighting
"                          ---------------------------------------
" 	required : 
" 		Plugin 'scrooloose/syntastic'
" ------------------------------------------------------------------------------------
let python_highlight_all=1
let g:syntastic_python_flake8_post_args='--ignore=E501'

" ------------------------------------------------------------------------------------
" 		   	       Features : Git integration
"                              --------------------------
" 	required : 
" 		Plugin 'tpope/vim-fugitive'
" 		Plugin 'airblade/vim-gitgutter'
"	commands :
"		:Gitstatus 					" git status command
"		:Gitcommit 					" git commit command
"		:Gitadd 					" git add command
" ------------------------------------------------------------------------------------



" ===================================================================================
" ===================================================================================
"  		                Full Stack Development IDE
" ===================================================================================
" ===================================================================================

" ------------------------------------------------------------------------------------
" 				   Feature : Folding code
" 	required : 
"		Plugin 'tmhedberg/SimpylFold'
"	commands :
"		za 				" fold/unfold
" ------------------------------------------------------------------------------------
set foldmethod=indent
set foldlevel=99
" let g:SimpylFold_docstring_preview=1 		" see the docstrings for folded code
" nnoremap <space> za 				" enable folding with the spacebar

