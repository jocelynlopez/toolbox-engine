#!/bin/bash


sudo -H pip3 install -r requirements.txt

if [ -z "$TOOLBOXDIR" ]; then
    echo "TOOLBOXDIR environment variable is unset !";
    while true; do
        read -p "Do you wish to install TOOLBOXDIR variable in bashrc set to : $PWD ?" yn
        case $yn in
            [Yy]* ) echo "export TOOLBOXDIR=$PWD" >> ~/.bashrc; echo "You need to restart this shell"; break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done

else
    cd /usr/bin

    if [ ! -z /usr/bin/toolbox ];then
        sudo rm -f /usr/bin/toolbox
    fi
    sudo ln -s $TOOLBOXDIR/toolbox.py toolbox
    
    if [ ! -z /usr/bin/toolbox-update ];then
        sudo rm -f /usr/bin/toolbox-update
    fi
    sudo ln -s $TOOLBOXDIR/update.sh toolbox-update

    echo "Installation of 'toolbox' and 'toolbox-update' command succeeded."
fi

